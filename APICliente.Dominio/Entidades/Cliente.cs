using System;
using System.Collections.Generic;

namespace APICliente.Dominio.Entidades
{
    public class Cliente : EntidadeBase
    {
        public string Nome { get; set; }
        public string Cpf { get; set; }
        public DateTime DataNascimento { get; set; }
        public int Idade { 
                get
                {
                    if (DateTime.Now.DayOfYear < DataNascimento.DayOfYear)
                        return DateTime.Now.Year - this.DataNascimento.Year - 1;
                    else 
                        return DateTime.Now.Year - this.DataNascimento.Year;
                } 
            }
        
        public List<Endereco> Enderecos { get; set; }

        public Cliente(string nome, string cpf, DateTime dataNascimento, int id)
        {
            cpf = cpf.Trim().Replace(".","").Replace(",","").Replace("-","");
            if (string.IsNullOrEmpty(nome) || nome.Length > 30) throw new ArgumentException("Nome é obrigatório e tamanho máximo de 50 caracteres !");
            if (string.IsNullOrEmpty(cpf) || cpf.Length != 11) throw new ArgumentException("Cpf é obrigatório !");
            if (dataNascimento == DateTime.MinValue) throw new ArgumentException("Data de nascimento é obrigatório !");
            
            this.Id = id;
            this.Nome = nome;
            this.Cpf = cpf;
            this.DataNascimento = dataNascimento;
        }

        public Cliente(int id)
        {
            if (id == 0) throw new ArgumentException("Informe um cliente !");
            this.Id = id;
        }

        
    }
}