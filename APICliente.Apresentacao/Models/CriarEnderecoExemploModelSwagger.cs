using Swashbuckle.AspNetCore.Filters;

namespace APICliente.Apresentacao.Models
{
    public class CriarEnderecoExemploModelSwagger : IExamplesProvider<EnderecoModel>
    {
        public EnderecoModel GetExamples()
        {
            return new EnderecoModel
            {
                Logradouro = "rua sei la das contas",
                Bairro = "em algum bairro",
                Cidade = "em alguma cidade",
                Estado = "em algum estado",
                ClienteId = 1
            };
        }
    }
}