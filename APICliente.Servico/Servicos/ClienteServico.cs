using System;
using System.Collections.Generic;
using System.Linq;
using APICliente.Dominio.Entidades;
using APICliente.Dominio.Interfaces.Repositorio;
using APICliente.Dominio.Interfaces.Servico;
using APICliente.Servico.Validators;

namespace APICliente.Servico.Servicos
{
    public class ClienteServico : IClienteServico
    {
        private readonly IClienteRepositorio _clienteRepositorio;
        private readonly IEnderecoRepositorio _enderecoRepositorio;
        public ClienteServico(IClienteRepositorio clienteRepositorio, IEnderecoRepositorio enderecoRepositorio)
        {
            _clienteRepositorio = clienteRepositorio ;
            _enderecoRepositorio = enderecoRepositorio;
        }
        public void Adicionar(Cliente entidade)
        {
            if (!ValidadorCpf.IsCpf(entidade.Cpf)) throw new ArgumentException("Cpf Inválido !");
            _clienteRepositorio.Adicionar(entidade);
        }

        public Cliente BuscarPorId(int id) =>
                                 _clienteRepositorio.BuscarPorId(id);
       
        public void Editar(Cliente entidade)
        {
            if (!ValidadorCpf.IsCpf(entidade.Cpf)) throw new ArgumentException("Cpf Inválido !");
            _clienteRepositorio.Editar(entidade);
        }

        public void Excluir(Cliente entidade)
        {
            if (entidade == null || entidade.Id == 0) throw new ArgumentNullException("Informe um cliente !");
            _clienteRepositorio.Excluir(entidade.Id);
            
        }

        public IList<Cliente> Listar() 
                            =>  _clienteRepositorio.Listar().ToList();
        
    }
}