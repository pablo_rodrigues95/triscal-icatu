using APICliente.Dominio.Entidades;

namespace APICliente.Dominio.Interfaces.Servico
{
    public interface IClienteServico : IBaseServico<Cliente>
    {
        
    }
}