using System.Collections.Generic;
using APICliente.Dominio.Entidades;

namespace APICliente.Dominio.Interfaces.Repositorio
{
    public interface IEnderecoRepositorio : IRepositorioBase<Endereco>
    {
        
    }
}