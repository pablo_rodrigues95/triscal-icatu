using APICliente.Dominio.Entidades;
using APICliente.Repositorio.EntityConfig;
using Microsoft.EntityFrameworkCore;

namespace APICliente.Repositorio.Context
{
    public class APIClienteContext : DbContext
    {
        public APIClienteContext(DbContextOptions<APIClienteContext> options) : base(options)
        {
        }
        public DbSet<Cliente> Cliente { get; set; }
        public DbSet<Endereco> Endereco { get; set; }

        
        protected override void OnModelCreating(ModelBuilder builder)
        {
           builder.ApplyConfiguration(new ClienteConfig());
           builder.ApplyConfiguration(new EnderecoConfig());
        }
        
        
    }
}