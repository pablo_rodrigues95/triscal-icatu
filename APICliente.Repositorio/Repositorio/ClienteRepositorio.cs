using System;
using System.Collections.Generic;
using System.Linq;
using APICliente.Dominio.Entidades;
using APICliente.Dominio.Interfaces.Repositorio;
using APICliente.Repositorio.Context;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace APICliente.Repositorio.Repositorio
{
    public class ClienteRepositorio : IClienteRepositorio
    {
        APIClienteContext _context;
        public ClienteRepositorio(APIClienteContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }
        public void Adicionar(Cliente entidade)
        {

            using(var cmd = _context.Database.GetDbConnection().CreateCommand())
            {
                cmd.CommandText = "INSERT INTO Cliente (Nome, Cpf, DataNascimento) OUTPUT INSERTED.Id VALUES ( @nome, @cpf , @dataNascimento )";
                cmd.CommandType = System.Data.CommandType.Text;
                if (cmd.Connection.State != System.Data.ConnectionState.Open) cmd.Connection.Open();
                cmd.Parameters.Add(new SqlParameter("@nome", entidade.Nome));
                cmd.Parameters.Add(new SqlParameter("@cpf", entidade.Cpf));
                cmd.Parameters.Add(new SqlParameter("@dataNascimento", entidade.DataNascimento));
                entidade.Id = (int)cmd.ExecuteScalar();
                cmd.Connection.Close();
            }
        }

        public Cliente BuscarPorId(int id)
        {
            string sql = "SELECT * FROM Cliente WHERE ID = @id";
            SqlParameter parametro = new SqlParameter("@id", id);
            return _context.Cliente.FromSqlRaw(sql, parametro).FirstOrDefault();
        }

        public void Editar(Cliente entidade)
        {
            string sql = "UPDATE Cliente set Nome = @nome , Cpf = @cpf, DataNascimento = @dataNascimento where Id = @id";
            List<SqlParameter> listaParametros = new List<SqlParameter>();
            listaParametros.Add(new SqlParameter("@nome", entidade.Nome));
            listaParametros.Add(new SqlParameter("@cpf", entidade.Cpf));
            listaParametros.Add(new SqlParameter("@dataNascimento", entidade.DataNascimento));
            listaParametros.Add(new SqlParameter("@id", entidade.Id));
            SqlParameter[] parametros = listaParametros.ToArray();
            _context.Database.ExecuteSqlRaw(sql, parametros);
            _context.SaveChanges();
        }

        public void Excluir(int id)
        {
            string sql = "DELETE FROM Cliente where Id = @id";
            SqlParameter parametro = new SqlParameter("@id", id);
            _context.Database.ExecuteSqlRaw(sql, parametro);
            _context.SaveChanges();

        }

        public IEnumerable<Cliente> Listar()
        {
            string sql = "SELECT * FROM Cliente";
            return _context.Cliente.FromSqlRaw(sql);
        }

    }
}