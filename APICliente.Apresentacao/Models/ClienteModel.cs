using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using APICliente.Dominio.Entidades;
using APICliente.Servico.Validators;

namespace APICliente.Apresentacao.Models
{
    public class ClienteModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Nome é Obrigatório !")]
        [MaxLength(30, ErrorMessage = "Nome possui tamanho máximo de 30 caracteres !")]
        public string Nome { get; set; }
        [Required(ErrorMessage = "Cpf é obrigátorio !")]
        public string Cpf { get; set; }
        [Required(ErrorMessage = "Data de nascimento é obrigatório !")]
        [ValidadorDataNascimento(ErrorMessage = "Data de nascimento inválida !")]
        public string DataNascimento { get; set; }
        public int Idade { get; set; }
        
        public static Cliente ModelParaEntidade(ClienteModel model)
        {
            return new Cliente(model.Nome, model.Cpf, Convert.ToDateTime(model.DataNascimento), model.Id);
        }

        public static ClienteModel EntidadeParaModel(Cliente cliente)
        {
            var model = new ClienteModel();
            model.Id = cliente.Id;
            model.Nome = cliente.Nome;
            model.Cpf = cliente.Cpf;
            model.Idade = cliente.Idade;
            model.DataNascimento = cliente.DataNascimento.ToShortDateString();
            return model;
        }

        public static IList<ClienteModel> ListaEntidadeParaModel(IList<Cliente> listaEntidade)
        {
            var listaModel = new List<ClienteModel>();
            foreach (var cliente in listaEntidade)
            {
                listaModel.Add(EntidadeParaModel(cliente));
            }
            return listaModel;
        }
    }
}