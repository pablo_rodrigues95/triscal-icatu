using Swashbuckle.AspNetCore.Filters;

namespace APICliente.Apresentacao.Models
{
    public class EditarEnderecoExemploModelSwagger : IExamplesProvider<EnderecoModel>
    {
        public EnderecoModel GetExamples()
        {
            return new EnderecoModel
            {
                Id = 1,
                Logradouro = "rua sei la das contas",
                Bairro = "em algum bairro",
                Cidade = "em alguma cidade",
                Estado = "em algum estado",
                ClienteId = 1
            };
        }
    }
}