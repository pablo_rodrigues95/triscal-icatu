using APICliente.Dominio.Entidades;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace APICliente.Repositorio.EntityConfig
{
    public class ClienteConfig : IEntityTypeConfiguration<Cliente>
    {
        public void Configure(EntityTypeBuilder<Cliente> builder)
        {
            builder.ToTable("Cliente");

            builder.HasKey(c => c.Id);

            builder.Property(c => c.Id).HasColumnName("Id").UseIdentityColumn(1,1).ValueGeneratedOnAdd();

            builder.Property(c => c.Nome).HasColumnName("Nome").HasColumnType("varchar(255)").HasMaxLength(30).IsRequired();

            builder.Property(c => c.Cpf).HasColumnName("Cpf").HasColumnType("varchar(255)").IsRequired();

            builder.Property(c => c.DataNascimento).HasColumnName("DataNascimento").HasColumnType("date").IsRequired();

            builder.HasMany(c => c.Enderecos).WithOne(e => e.Cliente).HasForeignKey(e => e.ClienteId).HasConstraintName("FK__Endereco__Client__4BAC3F29").OnDelete(DeleteBehavior.Cascade);

           

        }
    }
}