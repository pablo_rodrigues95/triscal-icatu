using APICliente.Dominio.Entidades;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace APICliente.Repositorio.EntityConfig
{
    public class EnderecoConfig : IEntityTypeConfiguration<Endereco>
    {
        public void Configure(EntityTypeBuilder<Endereco> builder)
        {
            builder.ToTable("Endereco");

            builder.HasKey(e => e.Id).HasName("Id");

            builder.Property(e => e.Id).HasColumnName("Id").UseIdentityColumn(1,1).ValueGeneratedOnAdd();

            builder.Property(e => e.Logradouro).HasColumnName("Logradouro").HasColumnType("varchar(255)").HasMaxLength(50).IsRequired();

            builder.Property(e => e.Bairro).HasColumnName("Bairro").HasColumnType("varchar(255)").HasMaxLength(40).IsRequired();

            builder.Property(e => e.Cidade).HasColumnName("Cidade").HasColumnType("varchar(255)").HasMaxLength(40).IsRequired();

            builder.Property(e => e.Estado).HasColumnName("Estado").HasColumnType("varchar(255)").HasMaxLength(40).IsRequired();
            
            builder.HasOne(e => e.Cliente).WithMany(c => c.Enderecos).HasForeignKey(e => e.ClienteId).HasConstraintName("FK__Endereco__Client__4BAC3F29").OnDelete(DeleteBehavior.Cascade);

        }
    }
}