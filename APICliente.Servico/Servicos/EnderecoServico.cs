using System;
using System.Collections.Generic;
using System.Linq;
using APICliente.Dominio.Entidades;
using APICliente.Dominio.Interfaces.Repositorio;
using APICliente.Dominio.Interfaces.Servico;

namespace APICliente.Servico.Servicos
{
    public class EnderecoServico : IEnderecoServico
    {
        IClienteRepositorio _clienteRepositorio;
        IEnderecoRepositorio _enderecoRepositorio;
        public EnderecoServico(IClienteRepositorio clienteRepositorio, IEnderecoRepositorio enderecoRepositorio)
        {
            _clienteRepositorio = clienteRepositorio ?? throw new ArgumentNullException(nameof(clienteRepositorio));
            _enderecoRepositorio = enderecoRepositorio ?? throw new ArgumentNullException(nameof(enderecoRepositorio));
        }

        public void Adicionar(Endereco entidade)
        {
            entidade.Cliente =_clienteRepositorio.BuscarPorId(entidade.ClienteId) ?? throw new ArgumentNullException("Informe um Cliente válido para poder adicionar um Endereço !");
            _enderecoRepositorio.Adicionar(entidade);
        }

        public Endereco BuscarPorId(int id) 
            => _enderecoRepositorio.BuscarPorId(id);
        

        public void Editar(Endereco entidade)
        {
            entidade.Cliente =_clienteRepositorio.BuscarPorId(entidade.ClienteId) ?? throw new ArgumentNullException("Informe um Cliente válido para poder editar um Endereço !");
            _enderecoRepositorio.Editar(entidade);
        }

        public void Excluir(Endereco entidade)
        {
            if (entidade == null || entidade.Id == 0) throw new ArgumentNullException("Informe um Endereço !");
            _enderecoRepositorio.Excluir(entidade.Id);
        }

        public IList<Endereco> Listar()
        {
            var enderecos = _enderecoRepositorio.Listar().ToList();
            foreach (var endereco in enderecos)
            {
                endereco.Cliente = _clienteRepositorio.BuscarPorId(endereco.ClienteId);
            }
            return enderecos;
        }
    }
}