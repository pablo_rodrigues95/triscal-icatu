using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using APICliente.Apresentacao.Models;
using APICliente.Dominio.Interfaces.Repositorio;
using APICliente.Dominio.Interfaces.Servico;

using APICliente.Repositorio.Context;
using APICliente.Repositorio.Repositorio;
using APICliente.Servico.Servicos;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Filters;

namespace APICliente.Apresentacao
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            
            services.AddControllers()
            .ConfigureApiBehaviorOptions(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
               
            });

            services.AddDbContext<APIClienteContext>(options =>
               options.UseSqlServer(
                   Configuration.GetConnectionString("DefaultConnection")),
                   ServiceLifetime.Scoped);

            services.AddTransient<IClienteServico, ClienteServico>();
            services.AddTransient<IClienteRepositorio, ClienteRepositorio>();
            
            services.AddTransient<IEnderecoServico, EnderecoServico>();
            services.AddTransient<IEnderecoRepositorio, EnderecoRepositorio>();

            services.AddSwaggerGen(options =>
              {
                options.ExampleFilters();
                options.EnableAnnotations();
                  options.SwaggerDoc(
                      "v1",
                      new OpenApiInfo
                      {
                          Version = "v1",
                          Title = "API Cliente",
                          Description = "Api de Clientes e Endereços"
                      });

                    var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                    var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                    options.IncludeXmlComments(xmlPath);
              });

            services.AddSwaggerExamplesFromAssemblies(Assembly.GetEntryAssembly());

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Api");
                c.RoutePrefix = string.Empty;
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
