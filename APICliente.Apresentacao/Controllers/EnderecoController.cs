using System.Net;
using System.Linq;
using APICliente.Apresentacao.Models;
using APICliente.Dominio.Interfaces.Servico;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Filters;
using Swashbuckle.AspNetCore.Annotations;
using System.Collections.Generic;
using APICliente.Dominio.Entidades;
using System;

namespace APICliente.Apresentacao.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EnderecoController : ControllerBase
    {
        private readonly IEnderecoServico _enderecoServico;
        public EnderecoController(IEnderecoServico enderecoServico)
        {
            _enderecoServico = enderecoServico;
        }

        /// <summary>
        /// Buscar endereço por id
        /// </summary>
        /// <response code="404">Endereço não encontrado !</response>
        [HttpGet("BuscarPorId")]
        [SwaggerResponse((int)HttpStatusCode.OK, "Endereço Encontrado !", typeof(ClienteModel))]
        [SwaggerResponse((int)HttpStatusCode.InternalServerError, "Erro interno do servidor  !")]
        public IActionResult BuscarPorId(int id)
        {
            try
            {
                var endereco = _enderecoServico.BuscarPorId(id);
                if (endereco == null) return NotFound("Endereço não encontrado !");
                var model = EnderecoModel.EntidadeParaModel(endereco);
                return new JsonResult(model);
            }
            catch (System.Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        /// <summary>
        /// Buscar todos endereços
        /// </summary>
        [HttpGet("Listar")]
        [SwaggerResponse((int)HttpStatusCode.OK, "Clientes encontrados !", typeof(List<EnderecoModel>))]
        [SwaggerResponse((int)HttpStatusCode.NoContent, "Não possui nenhum Cliente !")]
        [SwaggerResponse((int)HttpStatusCode.InternalServerError, "Erro interno do servidor  !")]
        public IActionResult Listar()
        {
            try
            {
                var enderecos = _enderecoServico.Listar();
                if (enderecos.Count == 0) return NoContent();
                var model = EnderecoModel.ListaEntidadeParaModel(enderecos);
                return new JsonResult(model);
            }
            catch (System.Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
        
        /// <summary>
        /// Criar Endereço
        /// </summary>
        /// <response code="400">Dados Inválidos !</response>
        [HttpPost("Criar")]
        [SwaggerRequestExample(typeof(EnderecoModel), typeof(CriarEnderecoExemploModelSwagger))]
        [SwaggerResponse((int)HttpStatusCode.OK, "Endereço criado com sucesso !", typeof(Endereco))]
        [SwaggerResponse((int)HttpStatusCode.InternalServerError, "Erro interno do servidor  !")]
        public IActionResult Criar([FromBody] EnderecoModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var endereco = EnderecoModel.ModelParaEntidade(model);
                _enderecoServico.Adicionar(endereco);
                return StatusCode(201, endereco);
            }
            catch (System.Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        /// <summary>
        /// Editar Endereço
        /// </summary>
        /// <response code="400">Dados Inválidos !</response>
        /// <response code="404">Endereço não encontrado !</response>
        [HttpPut("Editar")]
        [SwaggerResponse((int)HttpStatusCode.OK, "Endereço editado com sucesso !")]
        [SwaggerResponse((int)HttpStatusCode.InternalServerError, "Erro interno do servidor  !")]
        public IActionResult Editar([FromBody] EnderecoModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var endereco = _enderecoServico.BuscarPorId(Convert.ToInt32(model.Id));
                if (endereco == null) return NotFound("Endereço não encontrado !");
                _enderecoServico.Editar(EnderecoModel.ModelParaEntidade(model));
                return Ok("Endereço editado com sucesso !");
            }
            catch (System.Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        /// <summary>
        /// Excluir Endereço
        /// </summary>
        /// <response code="404">Endereço não encontrado !</response>
        [HttpDelete("Excluir")]
        [SwaggerResponse((int)HttpStatusCode.OK, "Endereço excluído com sucesso !")]
        [SwaggerResponse((int)HttpStatusCode.InternalServerError, "Erro interno do servidor  !")]
        public IActionResult Excluir([FromQuery] int id)
        {
            try
            {
                var endereco = _enderecoServico.BuscarPorId(id);
                if (endereco == null) return NotFound("Endereço não encontrado !");
                _enderecoServico.Excluir(endereco);
                return Ok("Endereço excluído com sucesso !");
            }
            catch (System.Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
        
        
    }
}