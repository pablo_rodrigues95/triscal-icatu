using System.Collections.Generic;
using System.Linq;
namespace APICliente.Dominio.Interfaces.Repositorio
{
    public interface IRepositorioBase<T> where T : class
    {
         void Adicionar(T entidade);
         void Editar(T entidade);
         void Excluir(int id);
         T BuscarPorId(int id);
         IEnumerable<T> Listar();
        
    }
}