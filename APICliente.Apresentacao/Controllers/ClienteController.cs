using System.Collections.Generic;
using System.Linq;
using System.Net;
using APICliente.Apresentacao.Models;
using APICliente.Dominio.Entidades;
using APICliente.Dominio.Interfaces.Servico;

using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using Swashbuckle.AspNetCore.Filters;

namespace APICliente.Apresentacao.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ClienteController : ControllerBase
    {
        private readonly IClienteServico _clienteServico;

        public ClienteController(IClienteServico clienteServico)
        {
            _clienteServico = clienteServico;
        }

        /// <summary>
        /// Buscar cliente por Id
        /// </summary>
        /// <response code="404">Cliente não encontrado !</response>
        [HttpGet("BuscarPorId")]
        [SwaggerResponse((int)HttpStatusCode.OK, "Cliente Encontrado !", typeof(ClienteModel))]
        [SwaggerResponse((int)HttpStatusCode.InternalServerError, "Erro interno do servidor  !")]
        public IActionResult BuscarPorId(int id)
        {
            try
            {
                var cliente = _clienteServico.BuscarPorId(id);
                if (cliente == null) return NotFound("Cliente não encontrado !");
                var model = ClienteModel.EntidadeParaModel(cliente);
                return new JsonResult(model);
            }
            catch (System.Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        /// <summary>
        /// Buscar todos clientes
        /// </summary>
        [HttpGet("Listar")]
        [SwaggerResponse((int)HttpStatusCode.OK, "Clientes encontrados !", typeof(List<ClienteModel>))]
        [SwaggerResponse((int)HttpStatusCode.NoContent, "Não possui nenhum Cliente !")]
        [SwaggerResponse((int)HttpStatusCode.InternalServerError, "Erro interno do servidor  !")]
        public IActionResult Listar()
        {
            try
            {
                var clientes = _clienteServico.Listar();
                if (clientes.Count == 0 ) return NoContent();
                var model = ClienteModel.ListaEntidadeParaModel(clientes);
                return Ok(model);
            }
            catch (System.Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        /// <summary>
        /// Criar Cliente
        /// </summary>
        /// <response code="400">Dados Inválidos !</response>
        [HttpPost("Criar")]
        [SwaggerRequestExample(typeof(ClienteModel), typeof(CriarClienteExemploModelSwagger))]
        [SwaggerResponse((int)HttpStatusCode.OK, "Cliente criado com sucesso !", typeof(Cliente))]
        [SwaggerResponse((int)HttpStatusCode.InternalServerError, "Erro interno do servidor  !")]
        public IActionResult Criar([FromBody] ClienteModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    var erros = ModelState.SelectMany(e => e.Value.Errors)
                                    .Select(x => x.ErrorMessage).ToArray();
                    return BadRequest(erros);
                } 
                var cliente = ClienteModel.ModelParaEntidade(model);
                _clienteServico.Adicionar(cliente);
                return StatusCode(201,cliente);
            }
            catch (System.Exception ex)
            {
                 return StatusCode(500, ex.Message);
            }
        }

        /// <summary>
        /// Editar Cliente
        /// </summary>
        /// <response code="400">Dados Inválidos !</response>
        /// <response code="404">Cliente não encontrado !</response>
        [HttpPut("Editar")]
        [SwaggerRequestExample(typeof(ClienteModel), typeof(EditarClienteExemploModelSwagger))]
        [SwaggerResponse((int)HttpStatusCode.OK, "Cliente editado com sucesso !")]
        [SwaggerResponse((int)HttpStatusCode.InternalServerError, "Erro interno do servidor  !")]
        public IActionResult Editar([FromBody] ClienteModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var cliente = _clienteServico.BuscarPorId(model.Id);
                if (cliente == null) return NotFound("Cliente não encontrado !");
                _clienteServico.Editar(ClienteModel.ModelParaEntidade(model));
                return Ok("Cliente editado com sucesso !");
            }
            catch (System.Exception ex)
            {
                 return StatusCode(500, ex.Message);
            }
        }

        /// <summary>
        /// Excluir Cliente
        /// </summary>
        /// <response code="404">Cliente não encontrado !</response>
        [HttpDelete("Excluir")]
        [SwaggerResponse((int)HttpStatusCode.OK, "Cliente excluído com sucesso !")]
        [SwaggerResponse((int)HttpStatusCode.InternalServerError, "Erro interno do servidor  !")]
        public IActionResult Excluir([FromQuery]int id)
        {
            try
            {
                var cliente = _clienteServico.BuscarPorId(id);
                if (cliente == null) return NotFound("Cliente não encontrado !");
                _clienteServico.Excluir(cliente);
                return Ok("Cliente excluído com sucesso !");
            }
            catch (System.Exception ex)
            {
                 return StatusCode(500, ex.Message);
            }
        }
    }
}