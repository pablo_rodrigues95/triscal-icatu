using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using APICliente.Dominio.Entidades;

namespace APICliente.Apresentacao.Models
{
    public class EnderecoModel
    {
        public int? Id { get; set; }
        [Required(ErrorMessage = "Logradouro é obrigatório !")]
        [MaxLength(50, ErrorMessage = "Logradouro possui tamanho máximo de 50 caracteres !")]
        public string Logradouro { get; set; }
        [Required(ErrorMessage = "Bairro é obrigatório !")]
        [MaxLength(40, ErrorMessage = "Bairro possui tamanho máximo de 40 caracteres !")]
        public string Bairro { get; set; }
        [Required(ErrorMessage = "Cidade é obrigatório !")]
        [MaxLength(40, ErrorMessage = "Cidade possui tamanho máximo de 40 caracteres !")]
        public string Cidade { get; set; }
        [Required(ErrorMessage = "Estado é obrigatório !")]
        [MaxLength(40, ErrorMessage = "Estado possui tamanho máximo de 40 caracteres !")]
        public string Estado { get; set; }
        [Required(ErrorMessage = "Informe um cliente para poder adicionar um endereço !")]
        public int ClienteId { get; set; }
        public ClienteModel Cliente { get; set; }
        

        public static Endereco ModelParaEntidade(EnderecoModel model)
        {
            return new Endereco(model.Logradouro, model.Bairro, model.Cidade, model.Estado, model.Id, new Cliente(model.ClienteId));
        }

        public static EnderecoModel EntidadeParaModel(Endereco endereco)
        {
            var model = new EnderecoModel();
            model.Id = endereco.Id;
            model.Logradouro = endereco.Logradouro;
            model.Bairro = endereco.Bairro;
            model.Cidade = endereco.Cidade;
            model.Estado = endereco.Estado;
            model.ClienteId = endereco.ClienteId;
            model.Cliente = ClienteModel.EntidadeParaModel(endereco.Cliente);
            return model;
        }

        public static IList<EnderecoModel> ListaEntidadeParaModel(IList<Endereco> lista)
        {
            var listaModel = new List<EnderecoModel>();
            foreach (var item in lista)
            {
                listaModel.Add(EntidadeParaModel(item));
            }
            return listaModel;
            
        }
        
    }
}