using System.Collections.Generic;

namespace APICliente.Dominio.Interfaces.Servico
{
    public interface IBaseServico<T> where T : class 
    {
         void Adicionar(T entidade);
         void Editar(T entidade);
         void Excluir(T entidade);
         T BuscarPorId(int id);
         IList<T> Listar();
    }
}