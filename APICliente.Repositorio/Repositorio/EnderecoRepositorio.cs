using System;
using System.Collections.Generic;
using System.Linq;
using APICliente.Dominio.Entidades;
using APICliente.Dominio.Interfaces.Repositorio;
using APICliente.Repositorio.Context;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace APICliente.Repositorio.Repositorio
{
    public class EnderecoRepositorio : IEnderecoRepositorio
    {

        APIClienteContext _context;
        public EnderecoRepositorio(APIClienteContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public void Adicionar(Endereco entidade)
        {
            using(var cmd = _context.Database.GetDbConnection().CreateCommand())
            {
                cmd.CommandText = "INSERT INTO Endereco (Logradouro, Bairro, Cidade, Estado, ClienteId) OUTPUT INSERTED.Id VALUES ( @logradouro, @bairro , @cidade, @estado, @clienteId )";
                cmd.CommandType = System.Data.CommandType.Text;
                if (cmd.Connection.State != System.Data.ConnectionState.Open) cmd.Connection.Open();
                cmd.Parameters.Add(new SqlParameter("@logradouro", entidade.Logradouro));
                cmd.Parameters.Add(new SqlParameter("@bairro", entidade.Bairro));
                cmd.Parameters.Add(new SqlParameter("@cidade", entidade.Cidade));
                cmd.Parameters.Add(new SqlParameter("@estado", entidade.Estado));
                cmd.Parameters.Add(new SqlParameter("@clienteId", entidade.Cliente.Id));
                entidade.Id = (int)cmd.ExecuteScalar();
                cmd.Connection.Close();
            }
        }

        public Endereco BuscarPorId(int id)
        {
            string sql = "SELECT * FROM Endereco WHERE ID = @id";
            SqlParameter parametro = new SqlParameter("@id", id);
            return _context.Endereco.FromSqlRaw(sql, parametro).FirstOrDefault();
        }

        public void Editar(Endereco entidade)
        {
            string sql = "UPDATE Endereco set Logradouro = @logradouro , Bairro = @bairro, Cidade = @cidade, Estado = @estado where Id = @id";
            List<SqlParameter> listaParametros = new List<SqlParameter>();
            listaParametros.Add(new SqlParameter("@logradouro", entidade.Logradouro));
            listaParametros.Add(new SqlParameter("@bairro", entidade.Bairro));
            listaParametros.Add(new SqlParameter("@cidade", entidade.Cidade));
            listaParametros.Add(new SqlParameter("@estado", entidade.Estado));
            listaParametros.Add(new SqlParameter("@id", entidade.Id));
            SqlParameter[] parametros = listaParametros.ToArray();
            _context.Database.ExecuteSqlRaw(sql, parametros);
            _context.SaveChanges();
        }

        public void Excluir(int id)
        {
            string sql = "DELETE FROM Cliente where Id = @id";
            SqlParameter parametro = new SqlParameter("@id", id);
            _context.Database.ExecuteSqlRaw(sql, parametro);
            _context.SaveChanges();
        }
        
        public IEnumerable<Endereco> Listar()
        {
            string sql = "SELECT * FROM Endereco";
            return _context.Endereco.FromSqlRaw(sql);
        }

    }
}