using System;

namespace APICliente.Dominio.Entidades
{
    public class Endereco : EntidadeBase
    {
        public string Logradouro { get; private set; }
        public string Bairro { get; private set; }
        public string Cidade { get; private set; }
        public string Estado { get; private set; }
        public int ClienteId { get; set; }
        public virtual Cliente Cliente { get; set; }

        private Endereco(string logradouro, string bairro, string cidade, string estado, int id)
        {
            this.Logradouro = logradouro;
            this.Bairro = bairro;
            this.Cidade = cidade;
            this.Estado = estado;
            this.Id = id;
            
        }
        public Endereco(string logradouro, string bairro, string cidade, string estado, int? id, Cliente cliente)
        {
            if (string.IsNullOrEmpty(logradouro) || logradouro.Length > 50) throw new ArgumentException("Logradouro é obrigatório e tamanho máximo de 50 caracteres !");
            if (string.IsNullOrEmpty(bairro) || bairro.Length > 40) throw new ArgumentException("Bairro é obrigatório e tamanho máximo de 50 caracteres !");
            if (string.IsNullOrEmpty(cidade) || cidade.Length > 40) throw new ArgumentException("Cidade é obrigatório e tamanho máximo de 40 caracteres !");
            if (string.IsNullOrEmpty(estado) || estado.Length > 40) throw new ArgumentException("Estado é obrigatório e tamanho máximo de 40 caracteres !");
            if (cliente == null || cliente.Id == 0) throw new ArgumentNullException("Informe um cliente para poder adicionar um endereço !");
            if (id != null && Convert.ToInt32(id) == 0) throw new ArgumentException("Informe um id para poder editar o endereço !");

            this.Logradouro = logradouro;
            this.Bairro = bairro;
            this.Cidade = cidade;
            this.Estado = estado;
            this.Id = Convert.ToInt32(id);
            this.ClienteId = cliente.Id;
            this.Cliente = cliente;
        }
        
        
    }
}