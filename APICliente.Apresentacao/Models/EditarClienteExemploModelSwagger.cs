using Swashbuckle.AspNetCore.Filters;

namespace APICliente.Apresentacao.Models
{
    public class EditarClienteExemploModelSwagger : IExamplesProvider<ClienteModel>
    {
        public ClienteModel GetExamples()
        {
            return new ClienteModel 
            {
                Id = 1,
                Nome = "Algum nome",
                Cpf = "314.354.190-30",
                DataNascimento  = "01/01/2000"
            };
        }
    }
}