using APICliente.Dominio.Entidades;

namespace APICliente.Dominio.Interfaces.Repositorio
{
    public interface IClienteRepositorio : IRepositorioBase<Cliente>
    {
                 
    }
}