using System;
using System.ComponentModel.DataAnnotations;

namespace APICliente.Servico.Validators
{
    public class ValidadorDataNascimento : ValidationAttribute
    {
        public ValidadorDataNascimento()
        {
        }

        public override bool IsValid(object value)
        {
            var dateString = value as string;
            DateTime result;
            var success = DateTime.TryParse(dateString, out result);
            if (success)
            {
                if(Convert.ToDateTime(dateString) >= DateTime.Now) return false;
            }
            return success;
        }
    }
}